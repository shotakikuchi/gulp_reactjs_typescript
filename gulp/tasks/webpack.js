var gulp = require("gulp");
var watch = require( 'gulp-watch' );
var webpack = require( 'webpack-stream' );
var webpackConfig = require('../../webpack.config.js');
//var uglify = require("gulp-uglify");
var plumber = require("gulp-plumber");
var config = require('../config');
var notify = require('gulp-notify');
var cache = require('gulp-cached');
var jshint = require('gulp-jshint');

//パス
var config = require('../config');

gulp.task('webpack', function () {
    gulp.src([config.path.src.ts + "/**/*.ts"])
        .pipe(cache('webpack'))
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(webpack(webpackConfig))
        .pipe(jshint())
        //.pipe(uglify())
        .pipe(gulp.dest(config.path.dest.js));
});