var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    uglify = require("gulp-uglify"),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream');
var config = require('../config');

// トランスパイル
gulp.task('browserify', function () {
    browserify(config.path.src.jsx, {debug: true})
        .transform(babelify, {presets: ['es2015', 'react']})
        .bundle()
        .on("error", function (err) {
            console.log("Error : " + err.message);
        })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(config.path.src.js))
        .on('end', function () {
            gulp.src([config.path.src.js  + "/bundle.js"])
                .pipe(plumber())
                .pipe(uglify({mangle: false}))
                .pipe(gulp.dest(config.path.dest.js))
        })
});