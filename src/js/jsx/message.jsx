// React をロード
import React from "react";

// Message クラス

export default class Message extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <p>
                こんにちは。{this.props.name} さん！
            </p>
        );
    }
}