// Reactをインポート
import React from "react";
import ReactDOM from "react-dom";

//外部ファイル
import Message from "./message.jsx"

//初期データ
var propsData = {name: 'Mineo Okuda'};

class App extends React.Component {
    // コンストラクター
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            name:"fafafa"
        };
    }

    // ボタンクリック時にcountに1を足す
    _onClick() {
        var _count = this.state.count + 1;
        this.setState({count: _count});
    }

    render() {
        return (
            <div>
                こんにちは、{this.props.data.name}さん
                <div>
                    {this.state.count}
                    <button onClick={this._onClick.bind(this)}>click</button>
                    <Message name={this.state.name}/>
                </div>
            </div>
        );
    }
}


ReactDOM
    .render(
        <App data={propsData}/>,
        document
            .getElementById(
                'container'
            )
    )
;