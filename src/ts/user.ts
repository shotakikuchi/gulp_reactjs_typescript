class User {
    constructor(
        private firstName:string = 'foo',
        private lastName:string = 'bar'
    ){}

    public getFullName():void {
        alert(this.firstName + ' ' + this.lastName);
    }
}

export default User;